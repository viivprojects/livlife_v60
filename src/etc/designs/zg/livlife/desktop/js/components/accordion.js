/**
 * Accordion
 */

(function ($) {
    "use strict";

    var api = {},
        refs = {
            showHideContentList: ".accordion-content-wrapper"
        };


    $('.accordion-head').on('click', function(){
       $(this).toggleClass('is-active').next('.accordion-content-wrapper').slideToggle(300);
    });

    // api.onHeaderClick = function (event, triggered) {
    //     var $this = $(this),
    //         $headParent = $this.parent(),
    //         height;
    //
    //     if (!$headParent.hasClass("is-active")) {
    //         var $inactivateSlide = $headParent.siblings(".is-active"),
    //             $showContent = $headParent.children(refs.showHideContentList),
    //             $hideContent = $headParent.siblings()
    //                 .children(refs.showHideContentList).not($showContent);
    //
    //         $hideContent.animate(
    //             {
    //                 height: 0
    //             }, {
    //                 duration: 300
    //             });
    //
    //         height = $showContent[0].scrollHeight;
    //         $showContent.css("height", 0);
    //         $headParent.addClass("is-active");
    //         $showContent.animate(
    //             {
    //                 height: height
    //             }, {
    //                 duration: 300,
    //                 complete: function () {
    //                     $inactivateSlide.removeClass("is-active");
    //                 }
    //             });
    //     }
    // };

    api.onRegister = function (element) {
        var $this = element.$scope,
            $slides = $(".accordion-container", $this).first().children(),
            $heads = $slides.children(".accordion-head");

        $this.on("click", ".accordion-head", api.onHeaderClick);
        $this.on("click", ".accordion-head > a", function (e) {
            e.preventDefault();
        });
        $heads.filter(".is-active").trigger("click", ["true"]);
    };

    Cog.registerComponent({
        name: "accordion",
        api: api,
        selector: ".accordion"
    });
})(Cog.jQuery());

