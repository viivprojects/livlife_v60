/**
 * GoogleMap
 *
 * https://developers.google.com/maps/documentation/javascript/reference
 */

(function ($) {
    "use strict";
    var api = {},
        defaultOptions = {
            height: 250,
            zoom: 14,
            latitude: 51.500134,
            longitude: -0.12623,
            mapType: "ROADMAP",
            markers: []
        };

    function showMarkers(scope) {
        if (!scope.mapOptions.markers.length) {
            return;
        }

        var marker, k, infoWindow, tmarker;

        // Create all giver markers for this map
        for (k in scope.mapOptions.markers) {
            // Create marker only if title is provided
            if (scope.mapOptions.markers.hasOwnProperty(k) &&
                scope.mapOptions.markers[k].title.length) {
                marker = scope.mapOptions.markers[k];
                // Marker object
                tmarker = new google.maps.Marker({
                    position: new google.maps.LatLng(marker.latitude, marker.longitude),
                    map: scope.gmap,
                    title: marker.title
                });

                // Add description window if provided
                if (marker.description.length) {
                    infoWindow = new google.maps.InfoWindow({
                        content: marker.description
                    });

                    // Popup window info after clicking marker
                    google.maps.event.addListener(
                        tmarker,
                        "click",
                        openInfoWindow(tmarker, infoWindow, scope.gmap)
                    );
                }

            }
        }
    }

    function openInfoWindow(tmarker, infoWindow, gMap) {
        return (function (tmarker, infoWindow) {
            return function () {
                infoWindow.open(gMap, tmarker);
            };
        })(tmarker, infoWindow);
    }

    api.onRegister = function (scope) {
        var browser = this.external.browser;
        //serve static map in ie7
        if (browser.msie && browser.version < 8) {
            return false;
        }

        var map = scope.$scope,
            options = $.extend(defaultOptions, map.data()),
            centerPosition;

        // Set individual height of each map
        map.height(options.height);

        // Set markerPosition object
        centerPosition = new google.maps.LatLng(options.latitude, options.longitude);

        // Set mapOption object to setup map
        scope.mapOptions = {
            zoom: options.zoom,
            center: centerPosition,
            markers: options.markers,
            mapTypeId: google.maps.MapTypeId[options.mapType]
        };

        // Create map
        scope.gmap = new google.maps.Map(map[0], scope.mapOptions);

        showMarkers(scope);


        // Check if map is placed inside tab
        var tabContent = map.parents(".tabs-content");
        if (tabContent.size()) {
            Cog.addListener("tab", "change", function (e) {
                if (tabContent.attr("id") === e.eventData.id) {
                    google.maps.event.trigger(scope.gmap, "resize");
                }
            });
        }
    };


    Cog.registerComponent({
        name: "map",
        api: api,
        selector: ".map-canvas",
        requires: [
            {
                name: "utils.browser",
                apiId: "browser"
            }
        ]
    });

})(Cog.jQuery());
