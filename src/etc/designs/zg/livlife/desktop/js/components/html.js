/**
 * HTML
 */



/**
 * Carousel
 */
(function ($) {
    "use strict";

    var api = {};

    $(function() {


        $('.richtext-references').on('click', 'p', function(){
            $('ol').slideToggle();
            $(this).parent().toggleClass('is-active');
        });

        /*####################  SET CHARTS DIAGNOSIS  ####################*/

        $('.diagnosis .chart.region_1,.diagnosis .chart.age_1,.diagnosis .chart.gender_1,.diagnosis .chart.sexual_1').easyPieChart({
            barColor: '#e2001d',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function(from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#e2001d';
                }
            }
        });

        $('.diagnosis .chart.region_2,.diagnosis .chart.age_2,.diagnosis .chart.gender_2,.diagnosis .chart.sexual_2').easyPieChart({
            barColor: '#126c81',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#126c81';
                }
            }
        });
        $('.diagnosis .chart.age_3,.diagnosis .chart.gender_3,.diagnosis .chart.sexual_3').easyPieChart({
            barColor: '#535353',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#535353';
                }
            }
        });
        $('.diagnosis .chart.sexual_4').easyPieChart({
            barColor: '#898989',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#898989';
                }
            }
        });

        /*####################  SET CHARTS DISCLOSURE  ####################*/

        $('.disclosure .chart.region_1,.disclosure .chart.age_1,.disclosure .chart.gender_1,.disclosure .chart.sexual_1').easyPieChart({
            barColor: '#e2001d',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function(from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#e2001d';
                }
            }
        });

        $('.disclosure .chart.region_2,.disclosure .chart.age_2,.disclosure .chart.gender_2,.disclosure .chart.sexual_2').easyPieChart({
            barColor: '#02c1c1',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#02c1c1';
                }
            }
        });
        $('.disclosure .chart.age_3,.disclosure .chart.gender_3,.disclosure .chart.sexual_3').easyPieChart({
            barColor: '#535353',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#535353';
                }
            }
        });
        $('.disclosure .chart.sexual_4').easyPieChart({
            barColor: '#898989',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#898989';
                }
            }
        });




        /*####################  SET CHARTS TREATMENT  ####################*/

        $('.treatment .chart.region_1,.treatment .chart.age_1,.treatment .chart.gender_1,.treatment .chart.sexual_1').easyPieChart({
            barColor: '#e2001d',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function(from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#e2001d';
                }
            }
        });

        $('.treatment .chart.region_2,.treatment .chart.age_2,.treatment .chart.gender_2,.treatment .chart.sexual_2').easyPieChart({
            barColor: '#f8c91a',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#f8c91a';
                }
            }
        });
        $('.treatment .chart.age_3,.treatment .chart.gender_3,.treatment .chart.sexual_3').easyPieChart({
            barColor: '#535353',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#535353';
                }
            }
        });
        $('.treatment .chart.sexual_4').easyPieChart({
            barColor: '#898989',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#898989';
                }
            }
        });


        /*####################  SET CHARTS DOCTOR  ####################*/

        $('.doctor .chart.region_1,.doctor .chart.age_1,.doctor .chart.gender_1,.doctor .chart.sexual_1').easyPieChart({
            barColor: '#e2001d',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function(from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#e2001d';
                }
            }
        });

        $('.doctor .chart.region_2,.doctor .chart.age_2,.doctor .chart.gender_2,.doctor .chart.sexual_2').easyPieChart({
            barColor: '#51a045',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#51a045';
                }
            }
        });
        $('.doctor .chart.age_3,.doctor .chart.gender_3,.doctor .chart.sexual_3').easyPieChart({
            barColor: '#535353',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#535353';
                }
            }
        });
        $('.doctor .chart.sexual_4').easyPieChart({
            barColor: '#898989',
            trackColor: '#e9e9e9',
            lineCap: 'round',
            lineWidth: 16,
            onStep: function (from, to, percent) {
                $(this.el).find('.chart-value').text(Math.abs(Math.round(percent)) + '%');
                if(percent == 0) {
                    this.options.barColor = this.options.trackColor;
                } else {
                    this.options.barColor = '#898989';
                }
            }
        });


        /*####################  END SET CHARTS   ####################*/

        //main function after user the filter
        function set_chart(chart_name, chart_value) {
            var chart = window.chart = $('.chart.' + chart_name).data('easyPieChart');
            chart.update(chart_value);
            if (chart_value == 0) {
                $('.' + chart_name + ' label').removeClass('active');

            } else {
                $('.' + chart_name + ' label').addClass('active');
            }

            console.log(chart_name,chart_value );
            if (chart_name.indexOf('region') > -1)
            {
                if (chart_value == 0 ) {
                    $('.icon_region').removeClass(chart_name);
                } else {
                    $('.icon_region').addClass(chart_name);
                }
            }
        }

        function set_chart_bar(chart_name, chart_value) {
            $('.chart-bar.' + chart_name + ' span').css("width", chart_value+"%");
            $('.' + chart_name).next('.percent').html(chart_value + '%');

        }

        var charts_values = new Array();
        var charts_bar_values = new Array();

/*####################  DIAGNOSIS  ####################*/


        function diagnosisChartsOne() {
            $('.icon_region .chart-box__1 label span').html('n=(413)');
            $('.icon_region .chart-box__2 label span').html('n=(346)');
            $('.age_1 label span').html('n=(221)');
            $('.age_2 label span').html('n=(316)');
            $('.age_3 label span').html('n=(222)');
            $('.gender_1 label span').html('n=(582)');
            $('.gender_2 label span').html('n=(172)');
            $('.gender_3 label span').html('n=(3)');
            $('.sexual .chart-box__1 label span').html('n=(191)');
            $('.sexual .chart-box__2 label span').html('n=(527)');
            $('.sexual .chart-box__3 label span').html('n=(34)');
            $('.sexual .chart-box__4 label span').html('n=(7)');
            $('.page-diagnosis .chart-content ul li').hide();
            $('.page-diagnosis .chart-content ul li:nth-child(1)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-68"},
                {name:"region_2", value:"-69"},
                {name:"age_1", value:"-76"},
                {name:"age_2", value:"-70"},
                {name:"age_3", value:"-60"},
                {name:"gender_1", value:"-70"},
                {name:"gender_2", value:"-63"},
                {name:"gender_3", value:"-43"}
                );
                for (var i=0; i < charts_values.length; i++)
                    set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"65"},
                {name:"sexual_2", value:"70"},
                {name:"sexual_3", value:"63"},
                {name:"sexual_4", value:"47"}
            );

            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.diagnosis__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();

        }

        function diagnosisChartsTwo() {
            $('.icon_region .chart-box__1 label span').html('n=(266)');
            $('.icon_region .chart-box__2 label span').html('n=(236)');
            $('.age_1 label span').html('n=(162)');
            $('.age_2 label span').html('n=(198)');
            $('.age_3 label span').html('n=(142)');
            $('.gender_1 label span').html('n=(422)');
            $('.gender_2 label span').html('n=(76)');
            $('.gender_3 label span').html('n=(1)');
            $('.sexual .chart-box__1 label span').html('n=(80)');
            $('.sexual .chart-box__2 label span').html('n=(399)');
            $('.sexual .chart-box__3 label span').html('n=(16)');
            $('.sexual .chart-box__4 label span').html('n=(7)');
            $('.page-diagnosis .chart-content ul li').hide();
            $('.page-diagnosis .chart-content ul li:nth-child(2)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-44"},
                {name:"region_2", value:"-47"},
                {name:"age_1", value:"-56"},
                {name:"age_2", value:"-44"},
                {name:"age_3", value:"-38"},
                {name:"gender_1", value:"-51"},
                {name:"gender_2", value:"-28"},
                {name:"gender_3", value:"-14"}
            );
            for (var i=0; i < charts_values.length; i++)
                 set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"27"},
                {name:"sexual_2", value:"53"},
                {name:"sexual_3", value:"30"},
                {name:"sexual_4", value:"47"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                 set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.diagnosis__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();

        }

        function diagnosisChartsThree() {
            $('.icon_region .chart-box__1 label span').html('n=(200)');
            $('.icon_region .chart-box__2 label span').html('n=(160)');
            $('.age_1 label span').html('n=(82)');
            $('.age_2 label span').html('n=(149)');
            $('.age_3 label span').html('n=(129)');
            $('.gender_1 label span').html('n=(282)');
            $('.gender_2 label span').html('n=(75)');
            $('.gender_3 label span').html('n=(1)');
            $('.sexual .chart-box__1 label span').html('n=(87)');
            $('.sexual .chart-box__2 label span').html('n=(249)');
            $('.sexual .chart-box__3 label span').html('n=(22)');
            $('.sexual .chart-box__4 label span').html('n=(2)');
            $('.page-diagnosis .chart-content ul li').hide();
            $('.page-diagnosis .chart-content ul li:nth-child(3)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-33"},
                {name:"region_2", value:"-32"},
                {name:"age_1", value:"-28"},
                {name:"age_2", value:"-33"},
                {name:"age_3", value:"-35"},
                {name:"gender_1", value:"-34"},
                {name:"gender_2", value:"-28"},
                {name:"gender_3", value:"-14"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"30"},
                {name:"sexual_2", value:"33"},
                {name:"sexual_3", value:"41"},
                {name:"sexual_4", value:"13"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.diagnosis__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();
        }
/*####################  END  ####################*/

/*####################  DISCLOSURE  ####################*/

        function disclosureChartsOne() {

            $('.icon_region .chart-box__1 label span').html('n=(160)');
            $('.icon_region .chart-box__2 label span').html('n=(137)');
            $('.age_1 label span').html('n=(93)');
            $('.age_2 label span').html('n=(116)');
            $('.age_3 label span').html('n=(88)');
            $('.gender_1 label span').html('n=(216)');
            $('.gender_2 label span').html('n=(80)');
            $('.gender_3 label span').html('n=(0)');
            $('.sexual .chart-box__1 label span').html('n=(86)');
            $('.sexual .chart-box__2 label span').html('n=(193)');
            $('.sexual .chart-box__3 label span').html('n=(14)');
            $('.sexual .chart-box__4 label span').html('n=(4)');
            $('.page-disclosure .chart-content ul li').hide();
            $('.page-disclosure .chart-content ul li:nth-child(1)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-26"},
                {name:"region_2", value:"-27"},
                {name:"age_1", value:"-32"},
                {name:"age_2", value:"-26"},
                {name:"age_3", value:"-24"},
                {name:"gender_1", value:"-26"},
                {name:"gender_2", value:"-29"},
                {name:"gender_3", value:"-0"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"29"},
                {name:"sexual_2", value:"26"},
                {name:"sexual_3", value:"26"},
                {name:"sexual_4", value:"27"}
            );

            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.disclosure__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();

        }

        function disclosureChartsTwo() {
            $('.icon_region .chart-box__1 label span').html('n=(381)');
            $('.icon_region .chart-box__2 label span').html('n=(328)');
            $('.age_1 label span').html('n=(182)');
            $('.age_2 label span').html('n=(290)');
            $('.age_3 label span').html('n=(237)');
            $('.gender_1 label span').html('n=(537)');
            $('.gender_2 label span').html('n=(168)');
            $('.gender_3 label span').html('n=(2)');
            $('.sexual .chart-box__1 label span').html('n=(172)');
            $('.sexual .chart-box__2 label span').html('n=(490)');
            $('.sexual .chart-box__3 label span').html('n=(39)');
            $('.sexual .chart-box__4 label span').html('n=(8)');
            $('.page-disclosure .chart-content ul li').hide();
            $('.page-disclosure .chart-content ul li:nth-child(2)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-62"},
                {name:"region_2", value:"-65"},
                {name:"age_1", value:"-63"},
                {name:"age_2", value:"-64"},
                {name:"age_3", value:"-64"},
                {name:"gender_1", value:"-65"},
                {name:"gender_2", value:"-62"},
                {name:"gender_3", value:"-29"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"59"},
                {name:"sexual_2", value:"66"},
                {name:"sexual_3", value:"72"},
                {name:"sexual_4", value:"53"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.disclosure__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();

        }

        function disclosureChartsThree() {
            $('.icon_region .chart-box__1 label span').html('n=(1)');
            $('.icon_region .chart-box__2 label span').html('n=(2)');
            $('.age_1 label span').html('n=(3)');
            $('.age_2 label span').html('n=(4)');
            $('.age_3 label span').html('n=(5)');
            $('.gender_1 label span').html('n=(6)');
            $('.gender_2 label span').html('n=(7)');
            $('.gender_3 label span').html('n=(8)');
            $('.sexual .chart-box__1 label span').html('n=(9)');
            $('.sexual .chart-box__2 label span').html('n=(10)');
            $('.sexual .chart-box__3 label span').html('n=(11)');
            $('.sexual .chart-box__4 label span').html('n=(12)');
            $('.page-disclosure .chart-content ul li').hide();
            $('.page-disclosure .chart-content ul li:nth-child(3)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-0"},
                {name:"region_2", value:"-0"},
                {name:"age_1", value:"-0"},
                {name:"age_2", value:"-0"},
                {name:"age_3", value:"-0"},
                {name:"gender_1", value:"-0"},
                {name:"gender_2", value:"-0"},
                {name:"gender_3", value:"-0"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"0"},
                {name:"sexual_2", value:"0"},
                {name:"sexual_3", value:"0"},
                {name:"sexual_4", value:"0"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.disclosure__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();
        }



        /*####################  TREATMENT  ####################*/


        function treatmentChartsOne() {
            $('.icon_region .chart-box__1 label span').html('n=(287)');
            $('.icon_region .chart-box__2 label span').html('n=(284)');
            $('.age_1 label span').html('n=(184)');
            $('.age_2 label span').html('n=(237)');
            $('.age_3 label span').html('n=(150)');
            $('.gender_1 label span').html('n=(441)');
            $('.gender_2 label span').html('n=(128)');
            $('.gender_3 label span').html('n=(1)');
            $('.sexual .chart-box__1 label span').html('n=(140)');
            $('.sexual .chart-box__2 label span').html('n=(400)');
            $('.sexual .chart-box__3 label span').html('n=(25)');
            $('.sexual .chart-box__4 label span').html('n=(6)');
            $('.page-treatment .chart-content ul li').hide();
            $('.page-treatment .chart-content ul li:nth-child(1)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-48"},
                {name:"region_2", value:"-57"},
                {name:"age_1", value:"-65"},
                {name:"age_2", value:"-53"},
                {name:"age_3", value:"-41"},
                {name:"gender_1", value:"-54"},
                {name:"gender_2", value:"-48"},
                {name:"gender_3", value:"-14"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"49"},
                {name:"sexual_2", value:"54"},
                {name:"sexual_3", value:"47"},
                {name:"sexual_4", value:"43"}
            );

            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.treatment__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();

        }

        function treatmentChartsTwo() {
            $('.icon_region .chart-box__1 label span').html('n=(240)');
            $('.icon_region .chart-box__2 label span').html('n=(220)');
            $('.age_1 label span').html('n=(83)');
            $('.age_2 label span').html('n=(185)');
            $('.age_3 label span').html('n=(192)');
            $('.gender_1 label span').html('n=(323)');
            $('.gender_2 label span').html('n=(133)');
            $('.gender_3 label span').html('n=(2)');
            $('.sexual .chart-box__1 label span').html('n=(136)');
            $('.sexual .chart-box__2 label span').html('n=(294)');
            $('.sexual .chart-box__3 label span').html('n=(27)');
            $('.sexual .chart-box__4 label span').html('n=(3)');
            $('.page-treatment .chart-content ul li').hide();
            $('.page-treatment .chart-content ul li:nth-child(2)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-54"},
                {name:"region_2", value:"-57"},
                {name:"age_1", value:"-48"},
                {name:"age_2", value:"-54"},
                {name:"age_3", value:"-60"},
                {name:"gender_1", value:"-54"},
                {name:"gender_2", value:"-58"},
                {name:"gender_3", value:"-40"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"57"},
                {name:"sexual_2", value:"54"},
                {name:"sexual_3", value:"63"},
                {name:"sexual_4", value:"27"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.treatment__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();

        }

        function treatmentChartsThree() {
            $('.icon_region .chart-box__1 label span').html('n=(207)');
            $('.icon_region .chart-box__2 label span').html('n=(148)');
            $('.age_1 label span').html('n=(78)');
            $('.age_2 label span').html('n=(144)');
            $('.age_3 label span').html('n=(133)');
            $('.gender_1 label span').html('n=(271)');
            $('.gender_2 label span').html('n=(80)');
            $('.gender_3 label span').html('n=(4)');
            $('.sexual .chart-box__1 label span').html('n=(83)');
            $('.sexual .chart-box__2 label span').html('n=(251)');
            $('.sexual .chart-box__3 label span').html('n=(16)');
            $('.sexual .chart-box__4 label span').html('n=(5)');
            $('.page-treatment .chart-content ul li').hide();
            $('.page-treatment .chart-content ul li:nth-child(3)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-46"},
                {name:"region_2", value:"-38"},
                {name:"age_1", value:"-45"},
                {name:"age_2", value:"-42"},
                {name:"age_3", value:"-41"},
                {name:"gender_1", value:"-45"},
                {name:"gender_2", value:"-35"},
                {name:"gender_3", value:"-80"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"35"},
                {name:"sexual_2", value:"46"},
                {name:"sexual_3", value:"37"},
                {name:"sexual_4", value:"45"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.treatment__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();
        }


        function treatmentChartsFour() {
            $('.icon_region .chart-box__1 label span').html('n=(1)');
            $('.icon_region .chart-box__2 label span').html('n=(2)');
            $('.age_1 label span').html('n=(3)');
            $('.age_2 label span').html('n=(4)');
            $('.age_3 label span').html('n=(5)');
            $('.gender_1 label span').html('n=(6)');
            $('.gender_2 label span').html('n=(7)');
            $('.gender_3 label span').html('n=(8)');
            $('.sexual .chart-box__1 label span').html('n=(9)');
            $('.sexual .chart-box__2 label span').html('n=(10)');
            $('.sexual .chart-box__3 label span').html('n=(11)');
            $('.sexual .chart-box__4 label span').html('n=(12)');
            $('.page-treatment .chart-content ul li').hide();
            $('.page-treatment .chart-content ul li:nth-child(3)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-23"},
                {name:"region_2", value:"-22"},
                {name:"age_1", value:"-19"},
                {name:"age_2", value:"-13"},
                {name:"age_3", value:"-15"},
                {name:"gender_1", value:"-44"},
                {name:"gender_2", value:"-47"},
                {name:"gender_3", value:"-34"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"19"},
                {name:"sexual_2", value:"24"},
                {name:"sexual_3", value:"30"},
                {name:"sexual_4", value:"18"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.treatment__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();
        }
        /*####################  END  ####################*/




        /*####################  DOCTOR  ####################*/


        function doctorChartsOne() {
            $('.icon_region .chart-box__1 label span').html('n=(381)');
            $('.icon_region .chart-box__2 label span').html('n=(408)');
            $('.age_1 label span').html('n=(190)');
            $('.age_2 label span').html('n=(323)');
            $('.age_3 label span').html('n=(276)');
            $('.gender_1 label span').html('n=(577)');
            $('.gender_2 label span').html('n=(205)');
            $('.gender_3 label span').html('n=(5)');
            $('.sexual .chart-box__1 label span').html('n=(218)');
            $('.sexual .chart-box__2 label span').html('n=(522)');
            $('.sexual .chart-box__3 label span').html('n=(39)');
            $('.sexual .chart-box__4 label span').html('n=(10)');
            $('.page-talking-with-your-doctor .chart-content ul li').hide();
            $('.page-talking-with-your-doctor .chart-content ul li:nth-child(1)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-62"},
                {name:"region_2", value:"-81"},
                {name:"age_1", value:"-66"},
                {name:"age_2", value:"-72"},
                {name:"age_3", value:"-74"},
                {name:"gender_1", value:"-70"},
                {name:"gender_2", value:"-75"},
                {name:"gender_3", value:"-71"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"74"},
                {name:"sexual_2", value:"70"},
                {name:"sexual_3", value:"72"},
                {name:"sexual_4", value:"67"}
            );

            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.doctor__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();

        }

        function doctorChartsTwo() {

            $('.icon_region .chart-box__1 label span').html('n=(124)');
            $('.icon_region .chart-box__2 label span').html('n=(82)');
            $('.age_1 label span').html('n=(71)');
            $('.age_2 label span').html('n=(91)');
            $('.age_3 label span').html('n=(44)');
            $('.gender_1 label span').html('n=(155)');
            $('.gender_2 label span').html('n=(47)');
            $('.gender_3 label span').html('n=(3)');
            $('.sexual .chart-box__1 label span').html('n=(52)');
            $('.sexual .chart-box__2 label span').html('n=(140)');
            $('.sexual .chart-box__3 label span').html('n=(8)');
            $('.sexual .chart-box__4 label span').html('n=(6)');
            $('.page-talking-with-your-doctor .chart-content ul li').hide();
            $('.page-talking-with-your-doctor .chart-content ul li:nth-child(2)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-29"},
                {name:"region_2", value:"-24"},
                {name:"age_1", value:"-35"},
                {name:"age_2", value:"-28"},
                {name:"age_3", value:"-17"},
                {name:"gender_1", value:"-27"},
                {name:"gender_2", value:"-24"},
                {name:"gender_3", value:"-60"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"25"},
                {name:"sexual_2", value:"26"},
                {name:"sexual_3", value:"21"},
                {name:"sexual_4", value:"67"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.doctor__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();

        }

        function doctorChartsThree() {
            $('.icon_region .chart-box__1 label span').html('n=(1)');
            $('.icon_region .chart-box__2 label span').html('n=(2)');
            $('.age_1 label span').html('n=(3)');
            $('.age_2 label span').html('n=(4)');
            $('.age_3 label span').html('n=(5)');
            $('.gender_1 label span').html('n=(6)');
            $('.gender_2 label span').html('n=(7)');
            $('.gender_3 label span').html('n=(8)');
            $('.sexual .chart-box__1 label span').html('n=(9)');
            $('.sexual .chart-box__2 label span').html('n=(10)');
            $('.sexual .chart-box__3 label span').html('n=(11)');
            $('.sexual .chart-box__4 label span').html('n=(12)');

            $('.page-talking-with-your-doctor .chart-content ul li').hide();
            $('.page-talking-with-your-doctor .chart-content ul li:nth-child(3)').show();
            charts_values = [];
            charts_values.push(
                {name:"region_1", value:"-33"},
                {name:"region_2", value:"-32"},
                {name:"age_1", value:"-29"},
                {name:"age_2", value:"-33"},
                {name:"age_3", value:"-35"},
                {name:"gender_1", value:"-34"},
                {name:"gender_2", value:"-27"},
                {name:"gender_3", value:"-14"}
            );
            for (var i=0; i < charts_values.length; i++)
                set_chart(charts_values[i]['name'], charts_values[i]['value']);

            charts_bar_values = [];
            charts_bar_values.push(
                {name:"sexual_1", value:"29"},
                {name:"sexual_2", value:"34"},
                {name:"sexual_3", value:"40"},
                {name:"sexual_4", value:"8"}
            );
            for (var z=0; z < charts_bar_values.length; z++)
                set_chart_bar(charts_bar_values[z]['name'], charts_bar_values[z]['value']);

            $('.diagnosis__filters input[type=checkbox]').prop('checked',true);
            $('.applied_filters > span').show();
        }
        /*####################  END  ####################*/








        $('.select .label').on('click', function() {
            if ($(this).parent().hasClass('active')) {
                $(this).next().slideUp();
                $(this).parent().removeClass('active');
            } else {
                $(this).next().slideDown();
                $(this).parent().addClass('active');
            }
        });

        //clear each of filters
        $('.applied_filters span span').on('click', function(){
            var target = $(this).parent().attr('class');
            $('.diagnosis__filters #' + target).trigger( "click" );
            $('.applied_filters_clear').show();
        });

        //clear all filters
        $('.applied_filters_clear').on('click', function(){
            $('.diagnosis__filters input[type=checkbox]').removeAttr('checked');
            $('.diagnosis__filters input[type=checkbox]').prop('checked',false);
            //filters_chart();
            $('.applied_filters > span').hide();
            set_chart('region_1', '0');
            set_chart('region_2', '0');
            set_chart('age_1', '0');
            set_chart('age_2', '0');
            set_chart('age_3', '0');
            set_chart('gender_1', '0');
            set_chart('gender_2', '0');
            set_chart('gender_3', '0');
            set_chart_bar('sexual_1', '0');
            set_chart_bar('sexual_2', '0');
            set_chart_bar('sexual_3', '0');
            set_chart_bar('sexual_4', '0');
        });


        function findElement(arr, propName, propValue) {
            for (var i=0; i < arr.length; i++)
                if (arr[i][propName] == propValue)
                    return arr[i];
        };

        $('.diagnosis__filters input[type=checkbox]').on('click', function (event) {
            var chart_name = ($(this).data('name'));

            if($(this).is(':checked')) {
                if (event.target.id.indexOf('sexual') > -1)
                {
                    var y = findElement(charts_bar_values, "name", chart_name);
                    set_chart_bar(chart_name, y["value"]);
                } else {
                    var x = findElement(charts_values, "name", chart_name);
                    set_chart(chart_name, x["value"]);
                }
                $('.applied_filters span.' + chart_name).show();
            } else {
                if (event.target.id.indexOf('sexual') > -1)
                {
                    var y = findElement(charts_bar_values, "name", chart_name);
                    set_chart_bar(chart_name, 0);
                } else {
                    var x = findElement(charts_values, "name", chart_name);
                    set_chart(chart_name, 0);
                }
                $('.applied_filters > span.' + chart_name).hide();
            }

        });



        /*####################  DIAGNOSIS START  ####################*/

        $('.page-diagnosis .charts-start .box').on('click', function (event) {
            if ($(this).hasClass('active')) {
                $('.charts-start .box').removeClass('active');
                $('.chart-content').slideUp(1450);
                set_chart('region_1', '0');
                set_chart('region_2', '0');
                set_chart('age_1', '0');
                set_chart('age_2', '0');
                set_chart('age_3', '0');
                set_chart('gender_1', '0');
                set_chart('gender_2', '0');
                set_chart('gender_3', '0');
                set_chart_bar('sexual_1', '0');
                set_chart_bar('sexual_2', '0');
                set_chart_bar('sexual_3', '0');
                set_chart_bar('sexual_4', '0');
            } else {
                $('.charts-start .box').removeClass('active');
                $(this).addClass('active');
                $('.chart-content').slideDown();
                if($(this).hasClass('start-chart-1')){
                    diagnosisChartsOne();
                } else if($(this).hasClass('start-chart-2')){
                    diagnosisChartsTwo();
                } else if($(this).hasClass('start-chart-3')){
                    diagnosisChartsThree();
                }
            }
        });

        /*####################  DIAGNOSIS START END  ####################*/

        /*####################  DISCLOSURE START  ####################*/

        $('.page-disclosure .charts-start .box').on('click', function (event) {
            if ($(this).hasClass('active')) {
                $('.charts-start .box').removeClass('active');
                $('.chart-content').slideUp(1450);
                set_chart('region_1', '0');
                set_chart('region_2', '0');
                set_chart('age_1', '0');
                set_chart('age_2', '0');
                set_chart('age_3', '0');
                set_chart('gender_1', '0');
                set_chart('gender_2', '0');
                set_chart('gender_3', '0');
                set_chart_bar('sexual_1', '0');
                set_chart_bar('sexual_2', '0');
                set_chart_bar('sexual_3', '0');
                set_chart_bar('sexual_4', '0');
            } else {
                $('.charts-start .box').removeClass('active');
                $(this).addClass('active');
                $('.chart-content').slideDown();
                if($(this).hasClass('start-chart-1')){
                    disclosureChartsOne();
                } else if($(this).hasClass('start-chart-2')){
                    disclosureChartsTwo();
                } else if($(this).hasClass('start-chart-3')){
                    disclosureChartsThree();
                }
            }
        });

        $('.page-treatment .charts-start .box').on('click', function (event) {
            if ($(this).hasClass('active')) {
                $('.charts-start .box').removeClass('active');
                $('.chart-content').slideUp(1450);
                set_chart('region_1', '0');
                set_chart('region_2', '0');
                set_chart('age_1', '0');
                set_chart('age_2', '0');
                set_chart('age_3', '0');
                set_chart('gender_1', '0');
                set_chart('gender_2', '0');
                set_chart('gender_3', '0');
                set_chart_bar('sexual_1', '0');
                set_chart_bar('sexual_2', '0');
                set_chart_bar('sexual_3', '0');
                set_chart_bar('sexual_4', '0');
            } else {
                $('.charts-start .box').removeClass('active');
                $(this).addClass('active');
                $('.chart-content').slideDown();
                if($(this).hasClass('start-chart-1')){
                    treatmentChartsOne();
                } else if($(this).hasClass('start-chart-2')){
                    treatmentChartsTwo();
                } else if($(this).hasClass('start-chart-3')){
                    treatmentChartsThree();
                } else if($(this).hasClass('start-chart-4')){
                    treatmentChartsFour();
                }

            }
        });



        /*####################  DOCTOR START END  ####################*/

        $('.page-talking-with-your-doctor .charts-start .box').on('click', function (event) {
            if ($(this).hasClass('active')) {
                $('.charts-start .box').removeClass('active');
                $('.chart-content').slideUp(1450);
                set_chart('region_1', '0');
                set_chart('region_2', '0');
                set_chart('age_1', '0');
                set_chart('age_2', '0');
                set_chart('age_3', '0');
                set_chart('gender_1', '0');
                set_chart('gender_2', '0');
                set_chart('gender_3', '0');
                set_chart_bar('sexual_1', '0');
                set_chart_bar('sexual_2', '0');
                set_chart_bar('sexual_3', '0');
                set_chart_bar('sexual_4', '0');
            } else {
                $('.charts-start .box').removeClass('active');
                $(this).addClass('active');
                $('.chart-content').slideDown();
                if($(this).hasClass('start-chart-1')){
                    doctorChartsOne();
                } else if($(this).hasClass('start-chart-2')){
                    doctorChartsTwo();
                } else if($(this).hasClass('start-chart-3')){
                    doctorChartsThree();
                }
            }
        });

        /*####################  DIAGNOSIS START END  ####################*/


    });
















    Cog.registerComponent({
        name: "diagnosis",
        api: api,
        selector: ".diagnosis",
        requires: [
            {
                name: "utils.status",
                apiId: "status"
            }
        ]
    });


})(Cog.jQuery());


(function ($) {
    "use strict";

    var api = {};

    $(function() {

        // header minimize after scrolling
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= 300) {
                $("#header").addClass("smallHeader");
            } else {
                $("#header").removeClass("smallHeader");
            }
        });
        //H2 in circle - separate words <br>
        $(".hero-homeTitle p").each(function() {
            var html = $(this).html().split("<br>");
            var newhtml = [];
            for(var i=0; i< html.length; i++) {
                //if(i>0 && (i%1) == 0)
                //newhtml.push("<br />");
                newhtml.push("<span class='part-"+[i]+"'>" + html[i] + "</span>");
            }
            $(this).html(newhtml.join(" "));
        });
        //H2 in circle - separate words <br>
        $(".hero-subpageTitle h2").each(function() {
            var html = $(this).html().split(" ");
            var newhtml = [];
            for(var i=0; i< html.length; i++) {
                //if(i>0 && (i%1) == 0)
                    //newhtml.push("<br />");
                newhtml.push("<span class='part-"+[i]+"'>" + html[i] + "</span>");
            }
            $(this).html(newhtml.join(" "));
        });


        //About PP - steering committee

        $('.committe-tile.alpha').on('click',function (e) {
            var main_block = $(this).closest('.person');
            var target = $(main_block).next().find('.person-one');

            if ($(target).hasClass('active')) {
                $(target).slideUp(0).removeClass('active');
                $('.committe-tile').removeClass('active');
            } else {
                $('.person-two, .person-one').slideUp(0).removeClass('active');
                $(target).slideDown(0).addClass('active');
                $('.committe-tile').removeClass('active');
                $(this).addClass('active');
            }

        });
        $('.committe-tile.omega').on('click',function (e) {
            var main_block = $(this).closest('.person');
            var target = $(main_block).next().find('.person-two');
            if ($(target).hasClass('active')) {
                $(target).slideUp(0).removeClass('active');
                $('.committe-tile').removeClass('active');
            } else {
                $('.person-one, .person-two').slideUp(0).removeClass('active');
                $('.committe-tile').removeClass('active');
                $(target).slideDown(0).addClass('active');
                $(this).addClass('active');
            }
        });

        var state = 0;
        var imgPixelsColor = new Array();
        var imgPixelsGrey = new Array();

        function gray(imgObj,z,check,length) {

            console.log("z"+z);
            console.log("check"+check);

            var canvas = document.createElement('canvas');
            var canvasContext = canvas.getContext('2d');

            var imgW = imgObj.width;
            var imgH = imgObj.height;
            canvas.width = imgW;
            canvas.height = imgH;

            canvasContext.drawImage(imgObj, 0, 0);


            if (state <length){
                imgPixelsColor[z] = canvasContext.getImageData(0, 0, imgW, imgH);
                imgPixelsGrey[z] = canvasContext.getImageData(0, 0, imgW, imgH);

                for (var y = 0; y <imgPixelsGrey[z].height; y++) {
                    for (var x = 0; x <imgPixelsGrey[z].width; x++) {
                        var i = (y * 4) * imgPixelsGrey[z].width + x * 4;
                        var avg = (imgPixelsGrey[z].data[i] + imgPixelsGrey[z].data[i + 1] + imgPixelsGrey[z].data[i + 2]) / 3;
                        imgPixelsGrey[z].data[i] = avg;
                        imgPixelsGrey[z].data[i + 1] = avg;
                        imgPixelsGrey[z].data[i + 2] = avg;
                    }
                }

                state++;
            }



            if( check == true){
                canvasContext.putImageData(imgPixelsColor[z],0,0,0,0, imgPixelsColor[z].width, imgPixelsColor[z].height);
                console.log("color");
            }else {
                canvasContext.putImageData(imgPixelsGrey[z], 0, 0, 0, 0, imgPixelsGrey[z].width, imgPixelsGrey[z].height);
                console.log("grey");
            }
            return canvas.toDataURL();
        }




        if (navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i) ){
            $("body").addClass("ie");

            $(".grayscale").each(function (i) {
                $(this).addClass("index-"+i);
            });

            var imgObj = document.querySelectorAll('.grayscale img');
            for (var i=0; i<imgObj.length;i++){
                imgObj[i].src = gray(imgObj[i],i,false,imgObj.length);
            }


            $(".grayscale").on({
                mouseover: function (e) {

                    var i= $(this).attr('class').match(/\d+$/)[0];
                        imgObj[i].src = gray(imgObj[i], i, true);

                },
                mouseout: function () {
                    var i= $(this).attr('class').match(/\d+$/)[0];
                    imgObj[i].src = gray(imgObj[i], i, false);
                }
            });


        };






    });

        Cog.registerComponent({
            name: "body",
            api: api,
            selector: "body",
            requires: [
                {
                    name: "utils.status",
                    apiId: "status"
                }
            ]
        });


})(Cog.jQuery());