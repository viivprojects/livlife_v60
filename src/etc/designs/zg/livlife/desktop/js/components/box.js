/**
 * Box
 */
(function ($) {



    appendContainer = $( "#movie-story-box" );

    $( ".box-story" ).click( function() {
        hiddenContent = $( this ).find( ".box-hidden" );
        if($(window).width() >= 767) {
            $(".movie-story-box-container").show();
            appendContainer.html( hiddenContent.clone() );// do your stuff
            $(".movie-story-box-container .close").show();
            $( ".box-story" ).removeClass( "active" ).addClass( "non-active" );
            $( this ).removeClass("non-active").addClass( "active" );
        } else {
            if ($( this ).hasClass( "active" )) {
                $( this ).removeClass( "active" );
                hiddenContent.hide();
            } else {
                $( this ).addClass( "active" );
                hiddenContent.show();
            }
        }

    } );

    $(".movie-story-box-container .close").on('click',function(){
        $( ".box-story" ).removeClass( "active" ).removeClass( "non-active" );
        $(".movie-story-box-container").hide();
    });


    appendContainer1 = $( "#movie-story-box-1" );

    $( ".box-movie" ).click( function() {
        hiddenContent1 = $( this ).find( ".box-hidden" );
        if($(window).width() >= 767){
            $( ".box-movie" ).removeClass( "active" ).addClass( "non-active" );
            $( this ).removeClass("non-active").addClass( "active" );
            $(".movie-story-box-container-1").show();
            appendContainer1.html( hiddenContent1.clone() );
            $(".movie-story-box-container-1 .close").show();
        } else {
            if ($( this ).hasClass( "active" )) {
                $( this ).removeClass( "active" );
                hiddenContent1.hide();
            } else {
                $( this ).addClass( "active" );
                hiddenContent1.show();
            }
        }

    } );

    $(".movie-story-box-container-1 .close").on('click',function(){
        $( ".box-movie" ).removeClass( "active" ).removeClass( "non-active" );
        $(".movie-story-box-container-1").hide();
    });

})(Cog.jQuery());

